php-ast (1.1.2-3) unstable; urgency=medium

  * Upload to unstable

 -- Ondřej Surý <ondrej@debian.org>  Fri, 27 Sep 2024 19:46:58 +0200

php-ast (1.1.2-3~exp4) experimental; urgency=medium

  * Rebuild with PHP 8.4.0RC1

 -- Ondřej Surý <ondrej@debian.org>  Fri, 27 Sep 2024 16:04:57 +0200

php-ast (1.1.2-3~exp3) experimental; urgency=medium

  * Fix Build-Depend on php-all-dev (>= 2:95~)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 24 Sep 2024 14:49:34 +0200

php-ast (1.1.2-3~exp2) experimental; urgency=medium

  * Upload to experimental

 -- Ondřej Surý <ondrej@debian.org>  Mon, 23 Sep 2024 18:49:38 +0200

php-ast (1.1.2-3~exp1) experimental; urgency=medium

  * Upload to experimental

 -- Ondřej Surý <ondrej@debian.org>  Mon, 23 Sep 2024 18:49:34 +0200

php-ast (1.1.2-3) unstable; urgency=medium

  * Bump Build-Depends to dh-php >= 5.5~

 -- Ondřej Surý <ondrej@debian.org>  Sun, 22 Sep 2024 08:12:42 +0200

php-ast (1.1.2-2) unstable; urgency=medium

  * Upload to experimental

 -- Ondřej Surý <ondrej@debian.org>  Sat, 21 Sep 2024 12:57:21 +0200

php-ast (1.1.2-1) unstable; urgency=medium

  * New upstream version 1.1.2

 -- Ondřej Surý <ondrej@debian.org>  Fri, 30 Aug 2024 15:37:20 +0200

php-ast (1.1.1-3) unstable; urgency=medium

  * Fix typo in d/control.in
  * Bump default version to PHP 8.4

 -- Ondřej Surý <ondrej@debian.org>  Sat, 06 Jul 2024 17:33:44 +0200

php-ast (1.1.1-2) unstable; urgency=medium

  * Fix typo in d/control.in
  * Bump default version to PHP 8.4

 -- Ondřej Surý <ondrej@debian.org>  Sat, 06 Jul 2024 17:33:31 +0200

php-ast (1.1.1-1) unstable; urgency=medium

  * New upstream version 1.1.1
  * Workaround the empty-binary-package lintian problem

 -- Ondřej Surý <ondrej@debian.org>  Sat, 25 Nov 2023 14:07:25 +0100

php-ast (1.1.0-2) unstable; urgency=medium

  * Regenerate d/control for PHP 8.2

 -- Ondřej Surý <ondrej@debian.org>  Fri, 09 Dec 2022 13:32:02 +0100

php-ast (1.1.0-1) unstable; urgency=medium

  * New upstream version 1.1.0
   + Upstream dropped support for PHP <= 7.1

 -- Ondřej Surý <ondrej@debian.org>  Fri, 09 Dec 2022 13:24:02 +0100

php-ast (1.0.16-4) unstable; urgency=medium

  * Fix the d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Mon, 03 Jan 2022 15:01:22 +0100

php-ast (1.0.16-3) unstable; urgency=medium

  * Regenerate d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Jan 2022 13:09:44 +0100

php-ast (1.0.16-1) unstable; urgency=medium

  * New upstream version 1.0.16

 -- Ondřej Surý <ondrej@debian.org>  Fri, 31 Dec 2021 09:14:44 +0100

php-ast (1.0.14-3) unstable; urgency=medium

  * Recompile for PHP 7.4 until the transition is complete

 -- Ondřej Surý <ondrej@debian.org>  Fri, 26 Nov 2021 11:25:48 +0100

php-ast (1.0.14-2) unstable; urgency=medium

  * Update the packaging to dh-php >= 4~

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Nov 2021 12:00:52 +0100

php-ast (1.0.14-1) unstable; urgency=medium

  * New upstream version 1.0.14

 -- Ondřej Surý <ondrej@debian.org>  Mon, 02 Aug 2021 08:56:30 +0200

php-ast (1.0.10-8) unstable; urgency=medium

  * Bump B-D to dh-php >= 3.1~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 18:06:37 +0100

php-ast (1.0.10-7) unstable; urgency=medium

  * Revert arch:all change, as it breaks shlibs:Depends

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 13:21:25 +0100

php-ast (1.0.10-6) unstable; urgency=medium

  * The main dummy package is arch:all
  * Bump dh-php Build-Depends to >= 3.0~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 07:50:27 +0100

php-ast (1.0.10-5) unstable; urgency=medium

  * Sync the changelog with Debian bullseye
  * Update d/gbp.conf for debian/main branch
  * Update standards version to 4.5.1 (no change)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Feb 2021 12:10:40 +0100

php-ast (1.0.10-4) unstable; urgency=medium

  * Update d/watch to use https://pecl.php.net
  * Lower the dh-php dependency to Debian sid version
  * Update d/gbp.conf for Debian bullseye

 -- Ondřej Surý <ondrej@debian.org>  Sun, 14 Feb 2021 16:39:36 +0100

php-ast (1.0.10-3) unstable; urgency=medium

  * Update for dh-php >= 2.0 support

 -- Ondřej Surý <ondrej@debian.org>  Sat, 17 Oct 2020 07:22:37 +0200

php-ast (1.0.10-2) unstable; urgency=medium

  * Don't fail on failed tests

 -- Ondřej Surý <ondrej@debian.org>  Mon, 12 Oct 2020 12:02:31 +0200

php-ast (1.0.10-1) unstable; urgency=medium

  * Finish conversion to debhelper compat level 10
  * New upstream version 1.0.10
   + Includes support for PHP 8.0

 -- Ondřej Surý <ondrej@debian.org>  Sat, 10 Oct 2020 21:23:48 +0200

php-ast (1.0.6-1) unstable; urgency=medium

  * New upstream version 1.0.6

 -- Ondřej Surý <ondrej@debian.org>  Mon, 02 Mar 2020 08:52:28 +0100

php-ast (1.0.5-1) unstable; urgency=medium

  * New upstream version 1.0.5

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 Dec 2019 11:46:35 +0100

php-ast (1.0.3-1) unstable; urgency=medium

  * Add Pre-Depends on php-common >= 0.69~
  * New upstream version 1.0.3

 -- Ondřej Surý <ondrej@sury.org>  Thu, 08 Aug 2019 08:22:05 +0200

php-ast (0.1.6-2) unstable; urgency=medium

  * Update Vcs-* to salsa.d.o
  * Update maintainer email to team+php-pecl@tracker.debian.org
    (Closes: #899964)

 -- Ondřej Surý <ondrej@debian.org>  Sun, 19 Aug 2018 12:04:39 +0000

php-ast (0.1.6-1) unstable; urgency=medium

  * Fix the README.md location (Closes: #887353)
  * New upstream version 0.1.6 (Closes: #887351)

 -- Ondřej Surý <ondrej@debian.org>  Thu, 01 Feb 2018 15:20:35 +0000

php-ast (0.1.5-1) unstable; urgency=medium

  * Change debian/watch to PECL
  * Switch packaging to PECL version
  * New upstream version 0.1.5

 -- Ondřej Surý <ondrej@debian.org>  Mon, 31 Jul 2017 19:01:54 +0200

php-ast (0.1.4-1) unstable; urgency=medium

  * Add d/watch
  * New upstream version 0.1.4

 -- Ondřej Surý <ondrej@debian.org>  Thu, 06 Jul 2017 14:55:46 +0200

php-ast (0.1.2-3) unstable; urgency=medium

  * Use custom d/rules with override to actually build anything usefull
    (Closes: #847293)

 -- Ondřej Surý <ondrej@debian.org>  Wed, 07 Dec 2016 10:34:22 +0100

php-ast (0.1.2-2) unstable; urgency=medium

  * Convert to dh_php package debian/rules

 -- Ondřej Surý <ondrej@debian.org>  Mon, 05 Dec 2016 11:55:41 +0100

php-ast (0.1.2-1) unstable; urgency=medium

  * Imported Upstream version 0.1.2

 -- Ondřej Surý <ondrej@debian.org>  Mon, 05 Dec 2016 11:38:12 +0100

php-ast (0.1.1-2) unstable; urgency=medium

  * Force rebuild with dh_php >= 0.7

 -- Ondřej Surý <ondrej@debian.org>  Mon, 29 Feb 2016 16:48:59 +0100

php-ast (0.1.1-1) unstable; urgency=low

  * Initial release

 -- Ondřej Surý <ondrej@debian.org>  Fri, 18 Dec 2015 15:43:00 +0100
